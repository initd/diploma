#!/bin/bash
exec /home/py_web/py_heap_sort/bin/gunicorn -w 5 --timeout 300 server:app --bind 127.0.0.1:10001 --pid /home/py_web/var/run/gunicorn/py_heap_sort_bottle.pid