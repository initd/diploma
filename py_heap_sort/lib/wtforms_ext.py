# coding: utf-8
from wtforms import fields
from wtforms import widgets

from py_heap_sort.lib import time_helper as th


def filter_strip(string):
    """Function instead of next lambda

        filter_strip = lambda x: x.strip() if x else None
        :param string: string to strip or None
        :return: stripped string
    """
    if string:
        return string.strip()


class TimestampField(fields.Field):
    widget = widgets.TextInput()

    def __init__(self, label=None, validators=None, fmt='%Y-%m-%d %H:%M:%S',
                 **kwargs):
        super(TimestampField, self).__init__(label, validators, **kwargs)
        self.format = fmt
        self.data = None

    def pre_validate(self, value):
        dt = th.date_to_dt(value.data.get("valid_thru"), date_fmt=self.format)
        self.data = th.dt_to_unixtime(dt)

    def _value(self):

        if self.raw_data:
            return ' '.join(self.raw_data)
        if self.data:
            return th.unixtime_to_date(self.data, datetime_fmt=self.format)
        return ""
