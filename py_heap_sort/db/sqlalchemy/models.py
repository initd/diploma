# coding: utf-8
import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import orm as sa_orm

from py_heap_sort import config
from py_heap_sort.db.sqlalchemy import connection
from py_heap_sort.lib import time_helper as th


convention = {
    "ix": 'ix_%(column_0_label)s',
    "uq": "uq_%(table_name)s_%(column_0_name)s",
    "ck": "ck_%(table_name)s_%(column_0_name)s",
    "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
    "pk": "pk_%(table_name)s"
}

metadata = sa.MetaData(naming_convention=convention)
Base = declarative_base(metadata=metadata)


class TestStatusesEnum(object):
    IN_PROGRESS = 0
    COMPLETED = 1

    STATUSES = {
        IN_PROGRESS: u"В процессе",
        COMPLETED: u"Завершён",
    }


class QuestionStatusesEnum(object):
    UNANSWERED = 0
    RIGHT = 1
    INVALID = 2
    SKIPPED = 3

    STATUSES = {
        UNANSWERED: u"Неотвеченный",
        RIGHT: u"Правильный",
        INVALID: u"Неверный",
        SKIPPED: u"Пропущен",
    }


class BaseMixin(object):
    time_created = sa.Column(sa.Integer, nullable=False,
                             default=lambda: th.unixtime_curr_utc())
    time_updated = sa.Column(sa.Integer,
                             default=lambda: th.unixtime_curr_utc(),
                             onupdate=lambda: th.unixtime_curr_utc())

    @staticmethod
    def session():
        return connection.get_scoped_session()

    @classmethod
    def get_items(cls, paginator=None, order_by=None, filter_by=None):
        result = cls.session().query(cls)
        if filter_by:
            for criteria in filter_by:
                result = result.filter_by(**criteria)
        if order_by is None:
            order_by = "id"
        result = result.order_by(getattr(cls, order_by))
        if paginator is None:
            current_offset = 1
            items_per_page = config.BL_PAGESIZE or 25
            result = result[current_offset:items_per_page]
        elif paginator == "all":
            result = result.all()
        else:
            result = result[paginator.current_offset:paginator.items_per_page]
        return result

    def save(self):
        self.session().commit()

    @property
    def datecreated(self):
        return th.unixtime_to_date(self.time_created, th.default_date_fmt)

    @property
    def dateupdated(self):
        return th.unixtime_to_date(self.time_updated, th.default_date_fmt)

    @property
    def timecreated(self):
        return th.unixtime_to_datetime_tz(
            self.time_created, th.default_datetime_fmt)

    @property
    def timeupdated(self):
        return th.unixtime_to_datetime_tz(
            self.time_updated, th.default_datetime_fmt)


class User(Base, BaseMixin):
    __tablename__ = "users"
    id = sa.Column(sa.Integer, primary_key=True, nullable=False)
    email = sa.Column(sa.Unicode(length=255), unique=True, nullable=False)
    password = sa.Column(sa.Unicode(length=64), nullable=False)
    time_activated = sa.Column(sa.Integer, default=0, nullable=False)
    time_locked = sa.Column(sa.Integer, default=0, nullable=False)
    time_deleted = sa.Column(sa.Integer, default=0, nullable=False)
    tests = sa_orm.relationship("Test", back_populates="user")
    graphic_tests = sa_orm.relationship("GraphicTest", back_populates="user")


class Test(Base, BaseMixin):
    __tablename__ = "tests"

    id = sa.Column(sa.Integer, primary_key=True, nullable=False)
    status = sa.Column(sa.Integer, default=0, nullable=False)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), index=True)
    user = sa_orm.relationship("User", back_populates="tests")
    questions = sa_orm.relationship("Question", back_populates="test")
    questions_count = sa.Column(sa.Integer, nullable=False)
    result = sa.Column(sa.Unicode(length=5), nullable=False, default=u"0.0")

    statuses_enum = TestStatusesEnum


class Question(Base, BaseMixin):
    __tablename__ = "questions"

    id = sa.Column(sa.Integer, primary_key=True, nullable=False)
    collection = sa.Column(sa.Unicode(length=1023), nullable=False)
    sort_step = sa.Column(sa.Integer, nullable=False)
    text = sa.Column(sa.Unicode(length=1023), nullable=False)
    status = sa.Column(sa.Integer, default=0, nullable=False)
    answers = sa_orm.relationship("Answer", back_populates="question")
    test_id = sa.Column(sa.Integer, sa.ForeignKey('tests.id'), index=True)
    test = sa_orm.relationship("Test", back_populates="questions")

    statuses_enum = QuestionStatusesEnum


class Answer(Base, BaseMixin):
    __tablename__ = "answers"

    id = sa.Column(sa.Integer, primary_key=True, nullable=False)
    text = sa.Column(sa.Unicode(length=1023), nullable=False)
    is_right = sa.Column(sa.Boolean, default=False, nullable=False)
    is_chosen = sa.Column(sa.Boolean, default=False, nullable=False)
    question_id = sa.Column(
        sa.Integer, sa.ForeignKey('questions.id'), index=True)
    question = sa_orm.relationship("Question", back_populates="answers")


class GraphicTest(Base, BaseMixin):
    __tablename__ = "graphic_tests"
    id = sa.Column(sa.Integer, primary_key=True, nullable=False)
    user_id = sa.Column(sa.Integer, sa.ForeignKey('users.id'), index=True)
    user = sa_orm.relationship("User", back_populates="graphic_tests")
    status = sa.Column(sa.Integer, default=0, nullable=False)
    collection = sa.Column(sa.Unicode(length=1023), nullable=False)
    current_step = sa.Column(sa.Integer, default=0, nullable=False)
    total_steps = sa.Column(sa.Integer, nullable=False)
    answers = sa_orm.relationship("GraphicAnswer", back_populates="test")
    result = sa.Column(sa.Unicode(length=5), nullable=False, default=u"0.0")

    statuses_enum = TestStatusesEnum


class GraphicAnswer(Base, BaseMixin):
    __tablename__ = "graphic_answers"
    id = sa.Column(sa.Integer, primary_key=True, nullable=False)
    text = sa.Column(sa.Unicode(length=1023), nullable=False)
    on_step = sa.Column(sa.Integer, nullable=False)
    is_right = sa.Column(sa.Boolean, default=False, nullable=False)
    is_chosen = sa.Column(sa.Boolean, default=False, nullable=False)
    test_id = sa.Column(
        sa.Integer, sa.ForeignKey('graphic_tests.id'), index=True)
    test = sa_orm.relationship("GraphicTest", back_populates="answers")
