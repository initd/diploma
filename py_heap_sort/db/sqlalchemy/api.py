# coding: utf-8
import contextlib

import sqlalchemy as sa
from sqlalchemy import orm as sa_orm

from py_heap_sort.db.sqlalchemy import connection
from py_heap_sort.db.sqlalchemy import models


ObjectNotFound = sa_orm.exc.NoResultFound


def get_scoped_session():
    return connection.get_scoped_session()


def remove_scoped_session():
    return connection.remove_scoped_session()


@contextlib.contextmanager
def request_session():
    session = get_scoped_session()
    try:
        yield session
        session().commit()
    finally:
        session().rollback()


def create_all():
    models.Base.metadata.create_all(connection.get_engine())


def get_items(model, paginator=None, order_by=None, filter_by=()):
    return getattr(models, model).get_items(
        paginator=paginator, order_by=order_by, filter_by=filter_by)


def user_create(**kwargs):
    with request_session() as session:
        new_user = models.User(**kwargs)
        session.add(new_user)
        return new_user


def users_get_count():
    with request_session() as session:
        return session.query(sa.func.count(models.User.id)).scalar()


def user_get_by_id(identifier):
    with request_session() as session:
        user = session.query(
            models.User).filter_by(id=identifier).one()
        return user


def user_get_by_email(email):
    with request_session() as session:
        user = session.query(models.User).filter_by(email=email).one_or_none()
        return user


def test_create(user, questions_count):
    session = connection.get_scoped_session()
    test = models.Test(
        user=user,
        questions_count=questions_count)
    session.add(test)
    return test


def tests_get_count():
    with request_session() as session:
        return session.query(sa.func.count(models.Test.id)).scalar()


def test_get_by_id(identifier):
    with request_session() as session:
        test = session.query(
            models.Test).filter_by(id=identifier).one()
        return test


def test_get_actual_by_user(user):
    with request_session() as session:
        test = session.query(
            models.Test).filter_by(
            user=user).filter_by(
            status=models.TestStatusesEnum.IN_PROGRESS).one_or_none()
        return test


def test_graphic_create(user, collection, total_steps):
    session = connection.get_scoped_session()
    test = models.GraphicTest(
        user=user,
        collection=collection,
        total_steps=total_steps
    )
    session.add(test)
    return test


def tests_graphic_get_count():
    with request_session() as session:
        return session.query(sa.func.count(models.GraphicTest.id)).scalar()


def test_graphic_get_by_id(identifier):
    with request_session() as session:
        test = session.query(
            models.GraphicTest).filter_by(id=identifier).one()
        return test


def test_graphic_get_actual_by_user(user):
    with request_session() as session:
        test = session.query(
            models.GraphicTest).filter_by(
            user=user).filter_by(
            status=models.TestStatusesEnum.IN_PROGRESS).one_or_none()
        return test


def question_create_for_test(test, text, collection, sort_step):
    session = connection.get_scoped_session()
    question = models.Question(
        test=test, text=text, collection=collection, sort_step=sort_step)
    session.add(question)
    return question


def question_get_by_id(identifier):
    with request_session() as session:
        question = session.query(
            models.Question).filter_by(id=identifier).one()
        return question


def questions_get_count_by_status_and_test(test, status):
    with request_session() as session:
        questions = session.query(
            models.Question).filter_by(test=test).filter_by(
            status=status).count()
        return questions


def questions_get_unanswered_by_test(test):
    with request_session() as session:
        questions = session.query(
            models.Question).filter_by(test=test).filter_by(
            status=models.QuestionStatusesEnum.UNANSWERED).all()
        return questions


def answer_create_for_question(question, text, is_right):
    session = connection.get_scoped_session()
    answer = models.Answer(question=question, text=text, is_right=is_right)
    session.add(answer)
    return answer


def answers_get_all_for_question(question):
    with request_session() as session:
        answers = session.query(
            models.Answer).filter_by(question=question).all()
        return answers


def answer_get_by_id(identifier):
    with request_session() as session:
        answer = session.query(
            models.Answer).filter_by(id=identifier).one()
        return answer


def answer_create_for_graphic_test(test, step, text, is_right):
    session = connection.get_scoped_session()
    answer = models.GraphicAnswer(
        test=test, on_step=step, text=text, is_right=is_right)
    session.add(answer)
    return answer


def answer_graphic_get_by_id(identifier):
    with request_session() as session:
        answer = session.query(
            models.GraphicAnswer).filter_by(id=identifier).one()
        return answer


def answers_graphic_delete_unselected_on_step(test, step):
    with request_session() as session:
        answers = session.query(
            models.GraphicAnswer).filter_by(
            test=test, on_step=step, is_chosen=False).delete()
        return answers


def answers_graphic_get_all_on_step(test, step):
    with request_session() as session:
        answers = session.query(
            models.GraphicAnswer).filter_by(
            test=test, on_step=step).all()
        return answers


def answers_graphic_get_all_right_answered(test):
    with request_session() as session:
        answers = session.query(
            models.GraphicAnswer).filter_by(
            test=test, is_right=True, is_chosen=True).count()
        return answers
