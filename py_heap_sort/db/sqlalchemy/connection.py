# coding: utf-8
import os

import sqlalchemy as sa
from sqlalchemy import orm as sa_orm

from py_heap_sort import config


connection_string = None
engine = None
scoped_session = None


def get_db_connection_string():
    global connection_string
    sqlite_connection_fmt = "sqlite:///{abs_path}"
    conn_fmt = "{db_type}://{username}:{password}@{host}:{port}/{database}"
    if connection_string is None:
        if config.DB_TYPE == "sqlite":
            connection_string = sqlite_connection_fmt.format(
                abs_path=os.path.join(config.DIR_ROOT, "db", config.DB_NAME))
        else:
            connection_string = conn_fmt.format(
                db_type=config.DB_TYPE,
                username=config.DB_USER,
                password=config.DB_PASS,
                host=config.DB_HOST,
                port=config.DB_PORT,
                database=config.DB_NAME,
            )
        return connection_string


def get_engine():
    global engine
    if engine is None:
        engine = sa.create_engine(get_db_connection_string(),
                                  echo=config.DB_DEBUG)
    return engine


def get_session():
    return sa_orm.sessionmaker(bind=get_engine())


def get_scoped_session():
    global scoped_session
    if scoped_session is None:
        scoped_session = sa_orm.scoped_session(session_factory=get_session())
    return scoped_session


def remove_scoped_session():
    scoped_session.remove()
