# coding: utf-8
from py_heap_sort.db.sqlalchemy import api as sa_api

IMPL = sa_api
ObjectNotFound = IMPL.ObjectNotFound


def get_scoped_session():
    return IMPL.get_scoped_session()


def remove_scoped_session():
    IMPL.remove_scoped_session()


def create_all():
    return IMPL.create_all()


def get_items(model, paginator=None, order_by=None, filter_by=()):
    return IMPL.get_items(model, paginator=paginator,
                          order_by=order_by, filter_by=filter_by)


def user_create(**kwargs):
    return IMPL.user_create(**kwargs)


def users_get_count():
    return IMPL.users_get_count()


def user_get_by_id(identifier):
    return IMPL.user_get_by_id(identifier)


def user_get_by_email(email):
    return IMPL.user_get_by_email(email)


def test_create(user, questions_count):
    return IMPL.test_create(user, questions_count)


def tests_get_count(is_grafic=False):
    if is_grafic:
        return tests_graphic_get_count()
    return IMPL.tests_get_count()


def test_get_by_id(identifier, is_graphic=False):
    if is_graphic:
        return test_graphic_get_by_id(identifier)
    return IMPL.test_get_by_id(identifier)


def test_get_actual_by_user(user):
    return IMPL.test_get_actual_by_user(user)


def test_graphic_create(user, collection, total_steps):
    return IMPL.test_graphic_create(user, collection, total_steps)


def tests_graphic_get_count():
    return IMPL.tests_graphic_get_count()


def test_graphic_get_by_id(identifier):
    return IMPL.test_graphic_get_by_id(identifier)


def test_graphic_get_actual_by_user(user):
    return IMPL.test_graphic_get_actual_by_user(user)


def question_create_for_test(test, text, collection, sort_step):
    return IMPL.question_create_for_test(
        test=test, text=text, collection=collection, sort_step=sort_step)


def question_get_by_id(identifier):
    return IMPL.question_get_by_id(identifier)


def questions_get_count_by_status_and_test(test, status):
    return IMPL.questions_get_count_by_status_and_test(test, status)


def questions_get_unanswered_by_test(test):
    return IMPL.questions_get_unanswered_by_test(test)


def answer_create_for_question(question, text, is_right):
    return IMPL.answer_create_for_question(
        question=question, text=text, is_right=is_right)


def answers_get_all_for_question(question):
    return IMPL.answers_get_all_for_question(question)


def answer_get_by_id(identifier):
    return IMPL.answer_get_by_id(identifier)


def answer_create_for_graphic_test(test, step, text, is_right):
    return IMPL.answer_create_for_graphic_test(
        test=test, step=step, text=text, is_right=is_right)


def answer_graphic_get_by_id(identifier):
    return IMPL.answer_graphic_get_by_id(identifier)


def answers_graphic_delete_unselected_on_step(test, step):
    return IMPL.answers_graphic_delete_unselected_on_step(test=test, step=step)


def answers_graphic_get_all_on_step(test, step):
    return IMPL.answers_graphic_get_all_on_step(test=test, step=step)


def answers_graphic_get_all_right_answered(test):
    return IMPL.answers_graphic_get_all_right_answered(test=test)
