# coding: utf-8
from __future__ import unicode_literals

import bcrypt
from time import time

T = int(time())


def gen_psw_hash(password):
    return bcrypt.hashpw(password, bcrypt.gensalt())

users = [
    dict(email="admin@ad.min",
         password=gen_psw_hash("admin").decode(),
         time_created=T,
         time_activated=T,),
]
