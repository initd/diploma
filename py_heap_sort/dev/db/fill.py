# coding: utf-8

from py_heap_sort import db as db_api
from py_heap_sort.dev.db.fixtures.users import users


def fill_users():
    for user in users:
        db_api.user_create(**user)


def all_data():
    db_api.create_all()
    db_api.IMPL.get_scoped_session()
    fill_users()
    db_api.IMPL.remove_scoped_session()


if __name__ == '__main__':
    all_data()
