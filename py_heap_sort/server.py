# coding: utf-8
from __future__ import print_function
from json import dumps as json_encode
import logging
import sys
import traceback

from bottle import default_app
from bottle import hook
from bottle import HTTPError
from bottle import HTTPResponse
from bottle import Jinja2Template
from bottle import response
from bottle import run
from bottle import static_file

from py_heap_sort.application.router import *  # noqa
from py_heap_sort.application.session import Session
from py_heap_sort import config
from py_heap_sort import db as db_api
from py_heap_sort.lib import time_helper as th


logger = logging.getLogger(__name__)
logger.addHandler(logging.StreamHandler())


@hook("before_request")
def before_request():
    request.time10 = th.unixtime_curr_utc()
    request.time13 = th.unixmtime_curr_utc()
    request.timezone = "GMT"
    request.logging = logging

    db_api.get_scoped_session()

    request.session = Session(
        request=request,
        response=response,
        auto_save=config.SESSION_AUTO_SAVE,
        auto_start=config.SESSION_AUTO_START,
        session_key=config.SESSION_KEY,
        session_salt=config.SESSION_SALT,
        session_time=config.SESSION_TIME,
        storage_type=config.SESSION_STORAGE,
        storage_settings={
            "db": config.REDIS_DB,
            "host": config.REDIS_HOST,
            "port": config.REDIS_PORT,
            "prefix": config.SESSION_PREFIX,
            "reverse_pref": config.SESSION_REVERS_PREFIX,
            "is_user_unique": config.SESSION_UNIQUE,
        },
    )


@hook("after_request")
def after_request():
    time_exec = (th.unixmtime_curr_utc() - request.time13) / 1000.
    response.headers["Access-Control-Allow-Origin"] = "*"
    response.headers["X-Time-Exec"] = "%.05f" % time_exec
    db_api.remove_scoped_session()
    request.session.process_autosave()


def errors_handler(callback):
    def print_traceback():
        """This method extract full callstack and return string representation

            :return string str: formatted callstack
        """
        exc = sys.exc_info()[0]
        stack = traceback.extract_stack()[:-1]
        if exc is not None:
            del stack[-1]
        trc = "Traceback (most recent call last):\n"
        stackstr = trc + "".join(traceback.format_list(stack))
        if exc is not None:
            stackstr += "  " + traceback.format_exc().lstrip(trc)
        print(stackstr)

    def wrapper(*args, **kwargs):
        try:
            return callback(*args, **kwargs)
        except HTTPError as e:
            print_traceback()
            return json_encode({"status": "error", "message": e.body})
        except HTTPResponse as e:
            raise e
        except Exception as e:
            print_traceback()
            return json_encode({"status": "error", "message": str(e)})
    return wrapper

"""
This is recursive import, that importing all routes from applications.
For understanding see applications/router/__init__.py.
"""
app = default_app()
app.install(errors_handler)

Jinja2Template.settings = {
    'autoescape': True,
    'extensions': ['jinja2.ext.autoescape']
}


@route('{0}/<path:path>'.format(config.STATIC_URL))
def static_server(path):
    return static_file(path, root=config.STATIC_PATH)

if __name__ == "__main__":
    run(app=app, debug=config.DEBUG_MODE, reloader=True,
        host=config.LISTEN_HOST, port=config.LISTEN_PORT)
else:
    if not config.DEBUG_MODE:
        app.catchall = False
