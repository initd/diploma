# coding: utf-8
from py_heap_sort.application.hotqueue.conn import messages_queue
from py_heap_sort.application.notificator import messages_senders


def main():
    email = messages_senders.Email()
    for message in messages_queue.consume():
        del message["type"]
        email.send_email(**message)


if __name__ == '__main__':
    main()
