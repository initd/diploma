# coding: utf-8
from __future__ import print_function

import functools
import json
import random

from py_heap_sort import config


def convert_collection_repr(collection):
    if isinstance(collection, basestring):
        return [int(item.strip()) for item in collection[1:-1].split(',')]
    return str(collection)


def gen_array(length=config.BL_DEMO_ARRAY_LENGTH,
              min_=config.BL_ARRAY_RANGE_MIN,
              max_=config.BL_ARRAY_RANGE_MAX,
              array=None):
    array = array or [random.randrange(min_, max_) for _ in range(length)]
    return array


def get_human_readable_op(additional=False):
    if config.BL_OP == ">":
        if additional:
            return u"по-возрастанию"
        return u"больше"
    if additional:
        return u"по-убыванию"
    return u"меньше"


def get_human_readable_message_for_init_step(step):
    msg = (
        u"Коллекция инициализирована и будет отсортирована {op}. ".format(
            op=get_human_readable_op(True),
        )
    )
    return msg


def get_human_readable_message_for_compare_step(step):
    msg = (
        u"Произведено сравнение ключей элементов "
        u"с индексами {first_index} и {second_index}, "
        u"элемент под индексом {result} {op}. ".format(
            first_index=step["kwargs"]["first_index"],
            second_index=step["kwargs"]["second_index"],
            result=step["result"],
            op=get_human_readable_op(),
        )
    )
    return msg


def get_human_readable_message_for_swap_step(step):
    if step['global_state_name'].startswith('main sort: swap sorted'):
        msg = (
            u"Произведен обмен отсортированного элемента "
            u"с вершины и элемента "
            u"с индексом {operated_child_index}, "
            u"соритируемая длина сокращена на 1. ".format(
                parent_index=step["kwargs"]["parent_index"],
                operated_child_index=step["kwargs"]["operated_child_index"]
            )
        )
        return msg
    msg = (
        u"Произведен обмен элементов "
        u"с индексами {parent_index} и {operated_child_index}, "
        u"так как элемент с индексом {operated_child_index} был {op}. ".format(
            parent_index=step["kwargs"]["parent_index"],
            operated_child_index=step["kwargs"]["operated_child_index"],
            op=get_human_readable_op(),
        )
    )
    return msg


def get_human_readable_message_for_complete_step(step):
    msg = (
        u"Коллекция успешно отсортирована {op}. ".format(
            op=get_human_readable_op(True),)
    )
    return msg


def append_status_to_human_readable_step(message, step):
    human_readable_state = u"Состояние: {collection}".format(
        collection=step["collection"]
    )
    return u"{}\n{}".format(message, human_readable_state)

human_readable_message_methods_mapping = {
    'Initial': get_human_readable_message_for_init_step,
    'get_index_of_operated_node': get_human_readable_message_for_compare_step,
    'swap_items': get_human_readable_message_for_swap_step,
    'Completed': get_human_readable_message_for_complete_step,
}


def get_human_readable_message_for_step(step, include_status=True):
    message = human_readable_message_methods_mapping[step['step_name']](step)
    if include_status:
        message = append_status_to_human_readable_step(message, step)
    return message


def step_deco(counter_name="compares_count"):
    def wrapper(method):
        @functools.wraps(method)
        def wrapped(self, *args, **kwargs):
            result = method(self, *args, **kwargs)
            self.step += 1
            setattr(self, counter_name, getattr(self, counter_name) + 1)
            self.add_step_to_history(
                step_name=method.__name__, kwargs=kwargs, result=result)
            return result
        return wrapped
    return wrapper


def convert_init_step(_):
    return {
        'type': 'init',
    }


def convert_compare_step(step):
    _step = {
        'type': 'focus',
        'first_index': step['kwargs']['first_index'],
        'second_index': step['kwargs']['second_index'],
        'first_key': step['collection'][step['kwargs']['first_index']],
        'second_key': step['collection'][step['kwargs']['second_index']],
        'result':
            u"да"
            if step['result'] == step['kwargs']['first_index']
            else u"нет",
    }
    return _step


def convert_swap_step(step):
    _step = {
        'type': 'swap',
        'first_index': step['kwargs']['parent_index'],
        'second_index': step['kwargs']['operated_child_index'],
        'first_key': step['collection'][step['kwargs']['parent_index']],
        'second_key':
            step['collection'][step['kwargs']['operated_child_index']],
    }
    if step['global_state_name'].startswith('main sort: swap sorted'):
        _step['change_active_length'] = -1
    return _step


def convert_complete_step(_):
    return {
        'type': 'change-active-length',
        'step': -1
    }


convert_methods_mapping_py2js = {
    'Initial': convert_init_step,
    'get_index_of_operated_node': convert_compare_step,
    'swap_items': convert_swap_step,
    'Completed': convert_complete_step,
}


def convert_step(step):
    result = convert_methods_mapping_py2js[step['step_name']](step)
    result.update({'data': step['collection']})
    return result


class HeapSorter(object):

    def __init__(self, collection, op=config.BL_OP, step_by_step_mode=False):
        self.collection = collection
        self.op = self.get_op(op)
        self.unsorted_len = len(self)
        self._step = 0
        self._step_by_step_mode = step_by_step_mode
        self._max_step = 0
        self.swaps_count = 0
        self.compares_count = 0
        self.global_state_name = "Generated"
        self.history = []
        self.add_step_to_history("Initial")

    def __getitem__(self, item):
        return self.collection[item]

    def __setitem__(self, key, value):
        self.collection[key] = value

    def __getattr__(self, item):
        return getattr(self.collection, item)

    def __len__(self):
        return len(self.collection)

    def __str__(self):
        return str(self.collection)

    def __repr__(self):
        return repr(self.collection)

    def __eq__(self, other):
        return self.collection == other

    @property
    def step(self):
        return self._step

    @step.setter
    def step(self, value):
        if self._step_by_step_mode and value > self._max_step:
            raise StopIteration
        self._step = value

    @staticmethod
    def get_op(op):
        if op == "<":
            return lambda first, second: first < second
        return lambda first, second: first > second

    @step_deco()
    def get_index_of_operated_node(self, first_index, second_index):
        if self.op(self[first_index], self[second_index]):
            return first_index
        return second_index

    def get_operated_child(self, index):
        left_child_index = index * 2 + 1
        right_child_index = index * 2 + 2
        if right_child_index > self.unsorted_len - 1:
            return left_child_index
        min_item_index = self.get_index_of_operated_node(
            first_index=left_child_index, second_index=right_child_index)
        return min_item_index

    @step_deco("swaps_count")
    def swap_items(self, parent_index, operated_child_index):
        self[parent_index], self[operated_child_index] = (
            self[operated_child_index], self[parent_index])

    def balance_sub_tree(self, parent_index):
        min_item_index = self.get_index_of_operated_node(
            first_index=parent_index,
            second_index=self.get_operated_child(parent_index))
        if min_item_index != parent_index:
            self.swap_items(parent_index=parent_index,
                            operated_child_index=min_item_index)
            return min_item_index
        return parent_index

    def sift(self, index):
        mid_length = self.unsorted_len / 2
        prev_index = None
        curr_index = index
        while (curr_index != prev_index) and (curr_index < mid_length):
            prev_index = curr_index
            curr_index = self.balance_sub_tree(parent_index=curr_index)

    def initial_heap_balance(self, length):
        self.global_state_name = "initial_balance_heap"
        starter_element = length / 2 - 1
        for index in range(starter_element, -1, -1):
            self.sift(index)

    def swap_sorted(self, last_sorted_index):
        self.global_state_name = "main sort: swap sorted"
        self.swap_items(parent_index=0, operated_child_index=last_sorted_index)
        self.unsorted_len -= 1

    def balance_heap_with_new_top(self):
        self.global_state_name = "main sort: balance heap with new top"
        self.sift(0)

    def main_sort(self, length):
        for index in range(length - 1, 0, -1):
            self.swap_sorted(index)
            self.balance_heap_with_new_top()

    def add_step_to_history(self, step_name, kwargs=None, result=None):
        if kwargs is None:
            kwargs = {}

        self.history.append({
            "collection": self.collection[:],
            "step_name": step_name,
            "step": self.step,
            "global_state_name": self.global_state_name,
            "kwargs": kwargs,
            "result": result,
            "actual_length": self.unsorted_len,
        })

    def output_history(self):
        for item in self.history:
            output = (
                "{step}: {collection}, unsorted_length: {actual_length}, "
                "{global_state_name}::{step_name}, "
                "{kwargs}, result: {result}".format(**item))
            print(output)
        print("Total compares:  {}".format(self.compares_count))
        print("Total swaps:     {}".format(self.swaps_count))
        print("Total steps:     {}".format(self.step))

    def get_human_readable_history(self):
        history = [get_human_readable_message_for_step(step)
                   for step in self.history]
        return history

    def convert_history_to_js(self, include_status=False):
        history = []
        for step in self.history:
            new_step = convert_step(step)
            new_step["message"] = get_human_readable_message_for_step(
                step, include_status)
            history.append(new_step)
        return json.dumps(history)

    def heap_sort(self):
        length = len(self)
        self.initial_heap_balance(length)
        self.main_sort(length)
        self.global_state_name = "Finished"
        self.step += 1
        self.add_step_to_history("Completed")
        return self

    def get_state_on_provided_step(self, step):
        self._step_by_step_mode = True
        self._max_step = step
        try:
            self.heap_sort()
            return self
        except StopIteration:
            return self


ARRAY = None


def init_array():
    global ARRAY
    ARRAY = gen_array()


def get_array_copy():
    return ARRAY[:]


def heap_sort_demo():
    sorter = HeapSorter(get_array_copy())
    sorter.heap_sort()


def builtin_sort_demo():
    get_array_copy().sort()


def main():
    debug = False

    def echo(msg):
        if debug:
            print(msg)

    test_array = [45, 40, 28, 25, 30, 44, 33, 22, 60, 15, 55, 47, 66, 20, 50]
    # DEBUG = False
    LENGTH = 100
    if debug:
        rand_array = gen_array(array=test_array)
    else:
        rand_array = gen_array(length=LENGTH)
    echo(rand_array)
    sorter = HeapSorter(rand_array[:])
    sorted_array = sorter.heap_sort()
    echo(sorted_array)
    built_in_sorted = sorted(rand_array)
    echo(built_in_sorted)
    assert built_in_sorted == sorted_array, "Error in algo"
    sorted_array.output_history()
    # rand_native_array = native_array.array('i', rand_array)
    # built_in_sorted = sorted(rand_native_array)
    # sorter = HeapSorter(rand_array)
    # sorted_array = sorter.heap_sort()
    # echo(sorted_array)
    # assert built_in_sorted == sorted_array, "Error in algo"

    print (rand_array)
    sorter = HeapSorter(rand_array[:])
    sorted_array = sorter.get_state_on_provided_step(10)
    sorted_array.output_history()
    print(sorted_array)

    print(rand_array)
    sorter = HeapSorter(rand_array[:])
    sorted_array = sorter.get_state_on_provided_step(200)
    sorted_array.output_history()
    print(sorted_array)

    array = [367, 949, 989, 198, 616, 992]
    sorter = HeapSorter(array[:])
    sorted_array = sorter.heap_sort()
    sorted_array.output_history()
    print(sorted_array)
    print(sorted_array.convert_history_to_js())
    print(sorted_array.get_human_readable_history())


if __name__ == '__main__':
    main()
