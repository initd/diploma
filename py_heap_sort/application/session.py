# coding: utf-8
from hashlib import md5
from random import randint
import time

from py_heap_sort.lib import session


# TODO(initd): fix bug with flash kinds: if we have !more than one! flash,
# flash can! change self KIND random, not that on adding,
# see on a rendered help page
class Session(session.Session):
    """Flashes kinds may be "success", "warning", "error", None - info"""
    delta_time_to_update_user_info = 30  # in seconds

    def __init__(self, *args, **kwargs):
        super(Session, self).__init__(*args, **kwargs)

    @property
    def flash_count(self):
        return len(self.data.get("flash", list()))

    def flash(self, message, kind=None, url=None):
        if not self.data.get("flash"):
            self.data["flash"] = list()
        self.data["flash"].append(dict(text=message, kind=kind, url=url))

    def flash_yield(self, kind=None):
        for i, message in enumerate(self.data.get("flash")):
            if kind and (kind == message["kind"]):
                del self.data["flash"][i]
                yield message
            elif not kind:
                del self.data["flash"][i]
                yield message

    def generate_token(self, token_type, keyword="email"):
        _str = "%s:%s:%s" % (self.session_salt, keyword, time.time())
        token_type = "token_%s" % token_type
        self.data[token_type] = md5(_str).hexdigest()[:20]
        if token_type == "token_phone":
            self.data[token_type] = "%05d" % randint(9999, 99999)
