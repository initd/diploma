# coding: utf-8
import datetime as dt

from bottle import jinja2_view as view
from bottle import route

from py_heap_sort import db as db_api
from py_heap_sort.lib.response import response_ok
from py_heap_sort.lib import time_helper as th


def days_to(days):
        return th.dt_to_unixtime(dt.datetime.now() + dt.timedelta(days=days))


@route("/")
@view("views/index.html")
def route_index():
    total_users = db_api.users_get_count()
    data = dict(total_users=total_users)
    return response_ok({"data": data})
