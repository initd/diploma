# coding: utf-8
from bottle import jinja2_view as view
from bottle import route

from py_heap_sort import db as db_api
from py_heap_sort.lib import auth
from py_heap_sort.lib.response import response_ok


@route("/admin")
@auth.require_auth
@auth.require_admin
@view("views/admin/index.html")
def route_admin_dashboard():
    total_users = db_api.users_get_count()
    data = dict(total_users=total_users)
    return response_ok({"data": data})
