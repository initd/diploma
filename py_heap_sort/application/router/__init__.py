# coding: utf-8
from py_heap_sort.application.router.admin.routes import *  # noqa
from py_heap_sort.application.router.dashboard.routes import *  # noqa
from py_heap_sort.application.router.heapsort.routes import *  # noqa
from py_heap_sort.application.router.users.routes import *  # noqa
