# coding: utf-8

from bottle import request
from wtforms import Form
from wtforms import PasswordField
from wtforms import StringField
from wtforms import ValidationError
from wtforms import validators

from py_heap_sort import db
from py_heap_sort.lib.wtforms_ext import filter_strip


class UserRegisterForm(Form):

    email = StringField(
        u"Email",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Email(message=u"Невалидный email"),
         validators.Length(min=6, message=u"Слишком короткий email")],
        filters=[filter_strip]
    )

    password = PasswordField(
        u"Пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5)],
        filters=[filter_strip]
    )

    def validate_email(self, field):
        if request.session.data.get("email") == field.data.lower():
            return True
        if db.user_get_by_email(email=field.data.lower()):
            raise ValidationError(
                u"Пользователь с такой почтой уже существует")


class UserLoginForm(Form):
    email = StringField(
        u"Email",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Email(message=u"Невалидный email"),
         validators.Length(min=6, message=u"Слишком короткий email")],
        filters=[filter_strip]
    )

    password = PasswordField(
        u"Пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5)],
        filters=[filter_strip]
    )


class UserPasswordRequestForm(Form):
    email = StringField(
        u"Email",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Email(message=u"Невалидный email"),
         validators.Length(min=6, message=u"Слишком короткий email")],
        filters=[filter_strip]
    )

    def validate_email(self, field):
        if not db.user_get_by_email(email=field.data.lower()):
            raise ValidationError(
                u"Пользователя с такой почтой не существует")


class UserVerifyForm(Form):
    token = StringField(
        u"Код верификации",
        [validators.DataRequired(message=u"Заполните поле")],
        filters=[filter_strip]
    )


class NewPasswordForm(UserVerifyForm):
    new_password = PasswordField(
        u"Новый пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5)]
    )


class UserPasswordChangeForm(Form):
    old_password = PasswordField(
        u"Старый пароль",
        [validators.DataRequired(message=u"Заполните поле")]
    )
    new_password = PasswordField(
        u"Новый пароль",
        [validators.DataRequired(message=u"Заполните поле"),
         validators.Length(min=5),
         validators.EqualTo("confirm", message=u"Пароли должны совпадать")]
    )

    confirm = PasswordField(
        u"Повторите пароль"
    )


class AdminUserChangeForm(Form):
    password = PasswordField(
        u"Новый пароль",
        filters=[filter_strip]
    )

    def validate_password(self, field):
        if field.data and len(field.data) < 5:
            raise ValidationError(
                u"Длина пароля должна составлять минимум 5 символов")
