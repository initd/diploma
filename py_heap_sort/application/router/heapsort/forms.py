# coding: utf-8
from bottle import request
import wtforms


class QuestionForm(wtforms.Form):

    answers = wtforms.RadioField(
        u"Выберите ответ",
        [wtforms.validators.InputRequired(
            message=u"Нужно выбрать хотя бы один вариант ответа.")],
        choices=[],
        coerce=int,
    )

    def validate_answers(self, field):
        field_data = field.data
        last_answers_ids = request.session.data.get("last_answers_ids", {})
        if field_data not in last_answers_ids:
            raise wtforms.ValidationError(u"Невалидный ID ответа.")
