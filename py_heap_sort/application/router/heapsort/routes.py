# coding: utf-8
import json

from bottle import HTTPResponse
from bottle import jinja2_template
from bottle import jinja2_view as view
from bottle import redirect
from bottle import request
from bottle import route

from py_heap_sort.application import heap_sort_algo
from py_heap_sort.application.questionary import api as quest_api
from py_heap_sort.application.router.heapsort import forms
from py_heap_sort import config
from py_heap_sort.db import api as db_api
from py_heap_sort.lib import auth
from py_heap_sort.lib import paginator as lib_paginator
from py_heap_sort.lib.response import response_ok


@route("/heapsort/demo")
@view("views/heapsort/demo.html")
def route_heap_sort_demo():
    array = heap_sort_algo.gen_array(config.BL_DEMO_ARRAY_LENGTH)
    sorter = heap_sort_algo.HeapSorter(array)
    result = sorter.heap_sort()
    result = result.convert_history_to_js()
    return response_ok({"operations_list": result,
                        "op_symbol": config.BL_OP})


@route("/heapsort/test")
@auth.require_auth
@view("views/heapsort/test.html")
def route_heap_sort_test():
    user_has_uncompleted_test = False
    user_has_uncompleted_graf_test = False
    curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
    if quest_api.get_uncompleted_test(curr_user):
        user_has_uncompleted_test = True
    if quest_api.get_uncompleted_test(curr_user, is_graphic=True):
        user_has_uncompleted_graf_test = True
    return response_ok({
        "user_has_uncompleted_test": user_has_uncompleted_test,
        "user_has_uncompleted_graf_test": user_has_uncompleted_graf_test})


@route("/heapsort/test", method="POST")
@auth.require_auth
def route_heap_sort_test_post():
    curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
    form = request.forms.decode()
    test_type = form.get("type")
    is_graphic = test_type == "graphic"
    if quest_api.get_uncompleted_test(curr_user, is_graphic=is_graphic):
        request.session.flash(u"Тест продолжен", "success")
    # if not exists, create new test and redirect to questions
    else:
        quest_api.start_test(curr_user, is_graphic=is_graphic)
        request.session.flash(u"Тест стартован", "success")
    if is_graphic:
        return redirect("/heapsort/test/graphic")
    return redirect("/heapsort/test/question")


@route("/heapsort/test/question")
@auth.require_auth
@view("views/heapsort/question.html")
def route_heap_sort_test_question(form=None):
    curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
    current_test = quest_api.get_uncompleted_test(curr_user)
    if not current_test:
        return redirect("/heapsort/test")
    # if there is an uncompleted test
    # get question from generated and show to user
    # else redirect to start test page
    try:
        question = quest_api.get_question(curr_user)
        request.session["last_question_id"] = question.id
        form = form or forms.QuestionForm()
        answers = quest_api.get_answers_for_question(question)

        request.session["last_answers_ids"] = set(
            [answer.id for answer in answers])
        form.answers.choices = [(answer.id, answer.text) for answer in answers]
        return response_ok({"form": form, "question": question})
    except IndexError:  # NoMoreQuestons
        quest_api.finish_test(curr_user, current_test)
        request.session.flash(u"Тест завершён", "success")
        return redirect("/heapsort/test/{test_id}/results".format(
            test_id=current_test.id))


@route("/heapsort/test/question/<identifier:int>", method="POST")
@auth.require_auth
@view("views/heapsort/question.html")
def route_heap_sort_test_question_post(identifier):
    # validate answer: get question from generated and validate answer id
    # check that question was not answered
    # analyze answer
    # save result
    form = forms.QuestionForm(request.forms.decode())
    form.answers.choices = [
        (item_id, item_id)
        for item_id in request.session.data.get("last_answers_ids")]
    if not form.validate():
        request.session.flash(u"Неверный id ответа.", "error")
        return route_heap_sort_test_question(form)
    if identifier != request.session.data.get("last_question_id"):
        request.session.flash(u"Неверный id вопроса.", "error")
        return redirect("/heapsort/test/question")
    answer_id = form.data.get("answers")
    curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
    try:
        quest_api.answer_question(curr_user, identifier, answer_id)
        request.session.flash(u"Ответ принят.")
    except ValueError:
        request.session.flash(
            u"Ответ не относится к данному вопросу.", "error")
    finally:
        return redirect("/heapsort/test/question")


@view("views/heapsort/graphic_results.html")
def show_graphic_test_result(data):
    return response_ok(data)


@view("views/heapsort/results.html")
def show_dynamic_test_result(data):
    return response_ok(data)


def show_test_result(identifier):
    is_graphic = "graphic" in request.path
    try:
        test = db_api.test_get_by_id(identifier, is_graphic=is_graphic)
    except db_api.ObjectNotFound:
        request.session.flash(u"Не найден тест с указанным ID.", "error")
        return redirect("/")
    # test_result = quest_api.calculate_test_result(test)
    data = {"test": test,
            "user": test.user,
            "result": test.result}
    if is_graphic:
        right_answered = db_api.answers_graphic_get_all_right_answered(test)
        data.update({"right_answered": right_answered})
        return show_graphic_test_result(data)
    return show_dynamic_test_result(data)


@route("/heapsort/test/<identifier:int>/results")
@route("/heapsort/test/graphic/<identifier:int>/results")
@auth.require_auth
def route_heap_sort_test_results(identifier):
    is_graphic = "graphic" in request.path
    try:
        curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
        test = db_api.test_get_by_id(identifier, is_graphic=is_graphic)
    except db_api.ObjectNotFound:
        request.session.flash(u"Не найден тест с указанным ID.", "error")
        return redirect("/")
    if test.user != curr_user:
        request.session.flash(
            u"Тест не существует, либо пройден другим.", "error")
        return redirect("/")
    return show_test_result(test.id)


@route("/admin/heapsort/test/<identifier:int>/results")
@route("/admin/heapsort/test/graphic/<identifier:int>/results")
@auth.require_auth
@auth.require_admin
def route_admin_heap_sort_test_results(identifier):
    return show_test_result(identifier)


@route("/admin/heapsort/tests/nav")
@route("/heapsort/tests/nav")
@auth.require_auth
@view("views/heapsort/tests_nav.html")
def route_nav_tests():
    return response_ok()


@route("/admin/heapsort/tests")
@route("/admin/heapsort/tests-<page:int>")
@route("/admin/heapsort/tests/graphic")
@route("/admin/heapsort/tests/graphic-<page:int>")
@auth.require_auth
@auth.require_admin
@view("views/heapsort/admin_tests.html")
def route_admin_tests_index(page=None):
    is_graphic = "graphic" in request.path
    suffix = '/graphic' if is_graphic else ''
    curr_ident = request.query.get("ident")
    if curr_ident is not None:
        message = u"Теста с id %s не существует"
        try:
            id_test = int(curr_ident)
            test = db_api.test_get_by_id(id_test, is_graphic=is_graphic)
            return redirect(
                "/admin/heapsort/test{suffix}/{ident}/results".format(
                    ident=test.id, suffix=suffix))
        except db_api.ObjectNotFound:
            request.session.flash(message % curr_ident)
            return redirect(
                "/admin/heapsort/tests{suffix}".format(suffix=suffix))

    page = 1 if not page else page
    template = "/admin/heapsort/tests%s-{page}" % suffix
    items_per_page = config.BL_ADMIN_ITEMS_PER_PAGE
    paginator = lib_paginator.Paginator(
        curr_page=page,
        template=template,
        items_per_page=items_per_page,
        total_items_count=db_api.tests_get_count(is_grafic=is_graphic))
    model_name = "GraphicTest" if is_graphic else "Test"
    tests = db_api.get_items(model_name, paginator=paginator)
    enumerated_tests = enumerate(tests, start=items_per_page * (page - 1) + 1)
    return response_ok({"enumerated_tests": enumerated_tests,
                        "paginator": paginator})


@route("/heapsort/tests")
@route("/heapsort/tests/graphic")
@route("/heapsort/tests-<page:int>")
@route("/heapsort/tests/graphic-<page:int>")
@auth.require_auth
@view("views/heapsort/user_tests.html")
def route_user_tests_index(page=None):
    is_graphic = "graphic" in request.path
    suffix = '/graphic' if is_graphic else ''
    page = 1 if not page else page
    template = "/heapsort/tests%s-{page}" % suffix
    items_per_page = config.BL_ITEMS_PER_PAGE
    paginator = lib_paginator.Paginator(
        curr_page=page,
        template=template,
        items_per_page=items_per_page,
        total_items_count=db_api.tests_get_count(is_grafic=is_graphic))
    curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
    model_name = "GraphicTest" if is_graphic else "Test"
    tests = db_api.get_items(model_name,
                             paginator=paginator,
                             filter_by=[{"user": curr_user}])
    enumerated_tests = enumerate(tests, start=items_per_page * (page - 1) + 1)
    return response_ok({"enumerated_tests": enumerated_tests,
                        "paginator": paginator})


def get_graphic_step_info(test):
    if not test:
        raise ValueError
    step = test.current_step
    total_steps = test.total_steps
    if step >= total_steps:
        quest_api.finish_graphic_test(test)
        raise IndexError
    right_answers_count = db_api.answers_graphic_get_all_right_answered(
        test)
    array = heap_sort_algo.convert_collection_repr(test.collection)
    sorter = heap_sort_algo.HeapSorter(array)
    result = sorter.get_state_on_provided_step(step)
    result = result.convert_history_to_js()
    answers = db_api.answers_graphic_get_all_on_step(test, step)
    answers = [(answer.id, answer.text) for answer in answers]
    request.session["last_answers_ids"] = dict(answers)
    return {
        "operations_list": result,
        "test_id": test.id,
        "current_step": step,
        "answers": answers,
        "total_questions": total_steps,
        "right_answers_count": right_answers_count}


@route("/heapsort/test/graphic")
@auth.require_auth
@view("views/heapsort/graphic_test.html")
def route_heap_sort_graphic_test():
    curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
    current_test = quest_api.get_uncompleted_test(curr_user, is_graphic=True)
    try:
        step_info = get_graphic_step_info(current_test)
    except ValueError:
        return redirect("/heapsort/test")
    except IndexError:
        return redirect(
            "/heapsort/test/graphic/{}/results".format(current_test.id))
    form = forms.QuestionForm()
    form.answers.choices = step_info['answers']
    step_info.update({"op_symbol": config.BL_OP, "form": form})
    return response_ok(step_info)


@route("/heapsort/test/graphic", method="POST")
@auth.require_auth
def route_heap_sort_graphic_test_post():
    curr_user = db_api.user_get_by_id(request.session.data.get("id_user"))
    current_test = quest_api.get_uncompleted_test(curr_user, is_graphic=True)
    form = forms.QuestionForm(request.forms.decode())
    form.answers.choices = [
        item_id
        for item_id in request.session.data.get("last_answers_ids").items()]
    if not form.validate():
        form_render = jinja2_template(
            "views/heapsort/graphic_include_form.html", form=form)
        msg = u"Неверный id ответа."
        body = json.dumps({"error": msg, "form_include": form_render})
        return HTTPResponse(body=body, status=400,
                            content_type='application/json')

    answer_id = form.data.get("answers")
    last_answer = quest_api.answer_graphic_test(answer_id, curr_user)
    try:
        result = get_graphic_step_info(current_test)
    except ValueError:
        return json.dumps({"redirect": "/heapsort/test"})
    except IndexError:
        return json.dumps(
            {"redirect": "/heapsort/test/graphic/{}/results".format(
                current_test.id)})

    form = forms.QuestionForm()
    form.answers.choices = result['answers']
    result['form'] = form
    result['last_is_right'] = last_answer.is_right
    render_result = jinja2_template(
        "views/heapsort/graphic_include.html", **result)
    form_render = jinja2_template(
        "views/heapsort/graphic_include_form.html", **result)
    return json.dumps({
        "operations_list": result["operations_list"],
        "status_include": render_result,
        "form_include": form_render}
    )
