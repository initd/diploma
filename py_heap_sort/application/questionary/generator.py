# coding: utf-8
import random

from py_heap_sort.application import heap_sort_algo
from py_heap_sort import config


def generate_random_steps(count, min, max, excluded):
    options = set(range(min, max))
    options.discard(excluded)
    result = set()
    for _ in range(count):
        random_step = random.choice(list(options))
        result.add(random_step)
        options.discard(random_step)
    return result


def gen_answers_for_question(sort_result, on_step, offset=1,
                             answers_count=config.BL_ANSWERS_PER_QUESTION):

        def gen_answer(step):
            return heap_sort_algo.get_human_readable_message_for_step(
                sort_result.history[step])

        def gen_right_answer():
            return heap_sort_algo.get_human_readable_message_for_step(
                sort_result.history[on_step + offset])

        steps = generate_random_steps(
            answers_count - 1, 1, len(sort_result.history), on_step + offset)
        answers = [{"text": gen_answer(chosen_step), "is_right": False}
                   for chosen_step in steps]
        answers.append({"text": gen_right_answer(), "is_right": True})
        random.shuffle(answers)
        return answers


def gen_question_locality_based(array, answers_count=None, prev_locality=None):
    # determinate behaviour
    if prev_locality is None:
        prev_locality = random.choice((True, False))
    offset = 1
    step_position_name = u"следующем"
    if prev_locality:
        offset = -1
        step_position_name = u"предыдущем"

    # sort array and choose one step from history
    sorter = heap_sort_algo.HeapSorter(array[:])
    sort_result = sorter.heap_sort()
    step = random.randrange(1, len(sort_result.history) - 1)
    # generate question
    question = {
        "text": u"Текущий шаг: {step} \n"
                u"Укажите операцию и состояние массива "
                u"на {step_position_name} шаге:".format(
                    step=heap_sort_algo.get_human_readable_message_for_step(
                        sort_result.history[step]),
                    step_position_name=step_position_name),
        "sort_step": step,
        "collection": str(array),
    }
    answers_count = answers_count or config.BL_ANSWERS_PER_QUESTION
    # generate answers to question
    generated_answers = gen_answers_for_question(
        sort_result, answers_count, offset, step)
    return question, generated_answers
