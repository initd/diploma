from py_heap_sort.application import heap_sort_algo
from py_heap_sort.application.questionary import generator
from py_heap_sort import config
from py_heap_sort.db import api as db_api


def get_uncompleted_test(user, is_graphic=False):
    if is_graphic:
        return db_api.test_graphic_get_actual_by_user(user)
    return db_api.test_get_actual_by_user(user)


def start_dynamic_test(user, questions_count=config.BL_QUESTIONS_IN_TEST):
    def _generate_question():
        # generate array
        array = heap_sort_algo.gen_array(config.BL_DEMO_ARRAY_LENGTH)
        question, answers = generator.gen_question_locality_based(array)
        # create question in db
        question_m = db_api.question_create_for_test(test=test, **question)
        for answer in answers:
            db_api.answer_create_for_question(question_m, **answer)
        return question_m

    if get_uncompleted_test(user):
        raise ValueError("There is an uncompleted test, "
                         "complete or delete it first")
    # create test in db
    test = db_api.test_create(user, questions_count)
    # generate questions with answers and save them into db
    for _ in range(questions_count):
        _generate_question()
    test.save()
    return test


def start_graphic_test(user):
    array = heap_sort_algo.gen_array(config.BL_DEMO_ARRAY_LENGTH)
    sorter = heap_sort_algo.HeapSorter(array[:])
    sort_result = sorter.heap_sort()
    total_steps = len(sort_result.history) - 1
    test = db_api.test_graphic_create(user, str(array), total_steps)
    # generate answers and save them into db
    for step in range(0, total_steps):
        answers = generator.gen_answers_for_question(
            sort_result, step)
        for answer in answers:
            db_api.answer_create_for_graphic_test(test, step, **answer)
    test.save()
    return test


def start_test(user, is_graphic=False):
    if is_graphic:
        return start_graphic_test(user)
    return start_dynamic_test(user)


def get_question(user):
    # check length of questions answered or raise
    # get one question with answers from db
    test = get_uncompleted_test(user)
    if not test:
        raise ValueError("There is no uncompleted tests")
    try:
        question = db_api.questions_get_unanswered_by_test(test)[0]
    except IndexError:
        raise IndexError("No questions left")
    return question


def get_answers_for_question(question):
    return db_api.answers_get_all_for_question(question)


def answer_question(user, question_id, given_answer_id):
    # analyze and change question status, mark answer as chosen one
    question = db_api.question_get_by_id(question_id)
    answer = db_api.answer_get_by_id(given_answer_id)
    if answer.question != question:
        raise ValueError("Question and answer mismatch.")
    if question.status != question.statuses_enum.UNANSWERED:
        raise ValueError("Question was answered lately.")
    if question.test.user != user:
        raise ValueError("User and question mismatch.")
    answer.is_chosen = True
    question.status = question.statuses_enum.INVALID
    if answer.is_right:
        question.status = question.statuses_enum.RIGHT
    question.test.result = calculate_test_result(question.test)
    question.save()


def answer_graphic_test(answer_id, user):
    answer = db_api.answer_graphic_get_by_id(answer_id)
    test = answer.test
    if test.user != user:
        raise ValueError("User and test answer mismatch")
    answer.is_chosen = True
    db_api.answers_graphic_delete_unselected_on_step(test, test.current_step)
    test.current_step += 1
    test.result = calculate_graphic_test_result(test)
    test.save()
    return answer


def finish_test(user, test):
    # check all questions answered
    if test.user != user:
        raise ValueError("User and test mismatch.")
    result = calculate_test_result(test)
    # write result into db
    # change status of the test
    test.result = result
    test.status = test.statuses_enum.COMPLETED
    test.save()
    return test


def finish_graphic_test(test):
    result = calculate_graphic_test_result(test)
    test.result = result
    test.status = test.statuses_enum.COMPLETED
    test.save()
    return test


def calculate_test_result(test):
    # return right_answers/test_length in percent
    # total question, right answered, invalid answered, skipped counts
    total_questions = float(test.questions_count)
    right_answered = db_api.questions_get_count_by_status_and_test(
        test, db_api.IMPL.models.QuestionStatusesEnum.RIGHT)
    right_percentage = str(right_answered / total_questions * 100)[:5]
    return right_percentage


def calculate_graphic_test_result(test):
    right_answered = float(db_api.answers_graphic_get_all_right_answered(test))
    right_percentage = str(right_answered / test.total_steps * 100)[:5]
    return right_percentage
