# coding: utf-8
import sys
import unittest


def new_behaviour():
    suite = unittest.TestLoader().discover('.')
    unittest.TextTestRunner(verbosity=2).run(suite)


def old_behaviour():
    verbosity = 1
    # result = unittest.TestResult()
    result = unittest.TextTestResult(
        stream=sys.stderr, descriptions=True, verbosity=verbosity
    )
    test_suite = unittest.TestLoader().discover('.')
    test_suite.run(result=result)

if __name__ == '__main__':
    old_behaviour()
    new_behaviour()
