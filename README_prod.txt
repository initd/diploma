﻿Допустим, что доменное имя хоста нашего приложения diploma.rodion.link. В качестве веб-сервера будет выступать NginX, в качестве wsgi-сервера — gunicorn, а в качестве сервера БД — PostgreSQL.


Подготовка к установке:
1) собрать пакет командой: python setup.py sdist
2) скопировать получившийся установочный пакет на целевую систему: scp dist/heap_sort-1.0.tar.gz user@diploma.rodion.link:/home/user/heap_sort-1.0.tar.gz
3) Залогиниться на сервер под пользователем user: ssh user@diploma.rodion.link


Установка (команды должны выполняться от суперпользователя, либо через sudo):
1) поставить необходимые вспомогательные пакеты: apt-get install supervisor python-dev python-pip nginx postgresql redis-server libpq-dev -y

2) убедится, что версия pip соответствует или меньше требуемой (6.1.1): pip -V, в случае, если версия не соответствует требованиям, установить, выполнив команду: pip install pip==6.1.1

3) pip install virtualenv

4) добавить пользователя py_web на целевую систему из под которого будет осуществляться работа сервиса: adduser py_web

5) создать виртуальное окружение для работы пакетов: virtualenv py_heap_sort (в домашней директории нового пользователя py_web)
6) активировать виртуальное окружение: source py_heap_sort/bin/activate
6) ещё раз проверить версию pip
7) установить пакет: pip install heap_sort-1.0.tar.gz (или .zip), при этом установятся все необходимые зависимости и создадутся следующие директории: 
a) /home/py_web/py_heap_sort/local/lib/python2.7/site-packages/py_heap_sort — директория с исходным/исполняемым кодом на python
b) /usr/share/py_heap_sort/py_heap_sort_static — директория со статическими файлами
c) /etc/supervisor — директория с конфигами для службы демонизатора приложений

8) скопировать пример конфига и отредактировать его с нужными вам настройками: cp /home/py_web/py_heap_sort/local/lib/python2.7/site-packages/py_heap_sort/config.example.py /home/py_web/py_heap_sort/local/lib/python2.7/site-packages/py_heap_sort/config.py

9) скопировать директорию со статическими файлами вместе с содержимым в директорию для раздачи статики веб-сервером nginx, предварительно создав нужную директорию: mkdir -p /var/www/diploma.rodion.link && cp -rp  /usr/share/py_heap_sort/py_heap_sort_static  /var/www/diploma.rodion.link/py_heap_sort_static

10) установить следующие права для директории: chown -R www-data:www-data /var/www/diploma.rodion.link/

11) подготовить конфиг nginx:
a) touch /etc/nginx/sites-available/diploma.rodion.link – создаем файл конфига
b) nano /etc/nginx/sites-available/diploma.rodion.link – открываем для редактирования
c) вносим туда содержимое: 
server {
	server_name diploma.rodion.link www.diploma.rodion.link;

	access_log /var/log/nginx/diploma.rodion.link.access.log;
	error_log /var/log/nginx/diploma.rodion.link.error.log;

	location  /py_heap_sort_static/ {
		root /var/www/diploma.rodion.link/;
	}

	location  / {
		proxy_set_header Host $host;
		proxy_set_header X-Real-IP $remote_addr;
		proxy_pass http://127.0.0.1:10001;
	}

}
d) создаём симлинк на конфиг, чтобы активировать конфигурацию: ln -s /etc/nginx/sites-available/diploma.rodion.link /etc/nginx/sites-enabled/diploma.rodion.link
e) перезапускаем nginx: service nginx reload

12) Подготовка wsgi-сервера:
a) убедиться, что сервер установлен: pip freeze | grep gunicorn
b) создать директорию для pid и дать права пользователю: mkdir -p  /home/py_web/var/run/gunicorn && chown -R py_web:py_web /home/py_web/var/run/gunicorn

13) Настройка БД:
a) авторизуемся под пользователем postgres: su -l postgres
b) входим в клиент базы данных: psql
c) создаем базу данных: CREATE DATABASE heap_sort_db;
d) создаем пользователя базы данных с паролем: CREATE USER heap_sort WITH password 'heap_sort_password';
e) даём права на созданную базу данных новому пользователю: GRANT ALL PRIVILEGES ON DATABASE heap_sort_db TO heap_sort;
f) редактируем файл настроек базы данных в соответствии с политиками безопасности: nano /etc/postgresql/9.4/main/pg_hba.conf, вносим туда следующую строчку-правило: local	heap_sort_db	heap_sort	md5
g) перезапускаем сервер базы данных: service postgresql reload ?(restart)
h) запустим скрипт инициализации данных приложения: heap_sort_db_init

14) Настройка демонизатора процессов supervisor:
a) создаем соответствующие папки логов для процессов: mkdir -p /var/log/supervisor/heap-sort-service
b) перзапускаем сервис демонизатора: supervisorctl reload

На этом установка будет завершена, можно будет открыть в браузере:  http://diploma.rodion.link для использования приложения.

Структура конфиг файла, необходимые параметры:
1. Секция Server содержит настройки bottle сервера
2. Секция DB содержит настройки подключения к базе данных
3. Секция Redis содержит настройки подключения к серверу key-value хранилища Redis
4. Секция Sessions содержит настройки сессий
5. Секция Secure содержит настройки, относящиеся к безопасности
6. Секция BL содержит настройки бизнес-логики приложения
7. Секция Queues содержит настройки очередей HotQueue
8. Секция Email содержит настройки подключения к почтовому серверу
9. Секция Async queue worker содержит настройку, включающую или отключающую асинхронную работу с блокирующими (либо долговыполняющимися) операциями
10. Секция Static files — секция настройки статических файлов


Зависимости указаны в файле requirements.txt, содержат следующие пакеты, для некоторых из которых закреплены версии:
bottle — fast, simple and lightweight WSGI micro web-framework 
gunicorn — WSGI HTTP Server for UNIX, based on pre-fork model
hotqueue — lightwight and fast queue library with backend on Redis
Jinja2==2.7.3 — full featured template engine
psycopg2 — PostgreSQL library for Python
py-bcrypt — bcrypt library
pytz — Olson timezone database library
redis — redis key-value NoSQL storage library for python
sqlalchemy – the Python SQL toolkit and Object Relational Mapper 
WTForms — flexible forms' validation and rendering library

Файлы-конфиги демонизатора Supervisor (heap-sort-message-sender.conf, heap-sort-server.conf) похожи и содержат следующие секции:
[program:heap-sort-message-sender] — заголовок секция с названием программы
directory=/home/py_web/py_heap_sort/local/lib/python2.7/site-packages/py_heap_sort — рабочая директория программы
user=py_web — пользователь, от которого будет выполнятся программа
group=py_web — группа, от которой будет выполнятся программа
command=/home/py_web/py_heap_sort/bin/python messager_queue_wrkr_start.py — команда старта программы
stdout_logfile=/var/log/supervisor/heap-sort-service/msg-sndr-logfile.log — логфайл, в который будет сохраняться стандартный вывод программы
stderr_logfile=/var/log/supervisor/heap-sort-service/msg-sndr-error.log — логфайл, в который будет сохраняться вывод ошибок программы
autostart=true — стартовать автоматически или только по команде пользователя
autorestart=true — рестартовать автоматически или только по команде пользователя
startsecs=5 — время, отпущенное на старт программы до полного функционирования

Деинсталляция (выполняется из-под суперпользователя, либо с помощью sudo):

1. Остановить сервисы в демонизаторе: supervisorctl stop all
2. Удалить пакет: pip uninstall heap-sort (из активированного виртуального окружения)
3. Удалить базу данных: DROP DATABASE heap_sort_db;
4. Удалить конфиг nginx: rm /etc/nginx/sites-enabled/diploma.rodion.link