
$(document).ready(function(){

    $("#valid_thru").datetimepicker({
        language: "ru",
        useSeconds: false,
        format: "YYYY-MM-DD HH:mm"
    });


    $("#admin_users_table").DataTable({paging: false, info: false});
    $("#admin_tests_table").DataTable({paging: false, info: false});
    $("#user_tests_table").DataTable({paging: false, info: false});  //, searching: false

    /* Handle "Find user" buttons */
    $(".user_search").click(function(event){
        var value = $(".user-ident-search-form").val();
        if (!value) {
            event.preventDefault();
            alertify.alert("Должен быть указан email или ID пользователя.");
        } else {
            $('<form id="form_search_user" method="GET" action="/admin/users"></form>').appendTo('body');
            form = $("#form_search_user");
            form.append('<input type="hidden" name="ident" value="' + value + '" />');
            form.submit();
        }
    });
    
    /* Handle "Find test" buttons */
    $(".test_search").click(function(event){
        var value = $(".test-ident-search-form").val();
        if (!value) {
            event.preventDefault();
            alertify.alert("Должен быть указан email или ID пользователя.");
        } else {
            form = $("#form_search_test");
            form.append('<input type="hidden" name="ident" value="' + value + '" />');
            form.submit();
        }
    });

});