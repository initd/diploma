
'use strict';

var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

var _createClass = function () {
  function defineProperties(target, props) {
    for (var i = 0; i < props.length; i++) {
      var descriptor = props[i];
      descriptor.enumerable = descriptor.enumerable || false;
      descriptor.configurable = true;
      if ("value" in descriptor) descriptor.writable = true;
      Object.defineProperty(target, descriptor.key, descriptor);
    }
  } return function (Constructor, protoProps, staticProps) {
    if (protoProps) defineProperties(Constructor.prototype, protoProps);
    if (staticProps) defineProperties(Constructor, staticProps);
    return Constructor;
  };
}();

function _classCallCheck(instance, Constructor) {
  if (!(instance instanceof Constructor)) {
    throw new TypeError("Cannot call a class as a function");
  }
}

var Visualizer = function () {
  function Visualizer(mountNode, initialData, options) {
    _classCallCheck(this, Visualizer);

    this.mountNode = mountNode;
    this.setInitialData(initialData);

    this.options = _extends({
      animationSpeed: 500,
      width: 600,
      height: 400,
      arrayCellHeight: 30,
      arrayCellWidth: 40,
      treeWidth: 650,
      treeCirclesRadius: 30,
      treeLevelsVerticalPadding: 15
    }, options || {});
    this._focusedIndexes = {};
    this._initContainers();
  }

  _createClass(Visualizer, [{
    key: 'setInitialData',
    value: function setInitialData(initialData) {
      this._data = initialData.map(function (val, i) {
        return { val: val, key: i };
      });
      this.activeLength = this._data.length;
    }
  }, {
    key: '_initContainers',
    value: function _initContainers() {
      this.svgContainer = d3.select(this.mountNode).append('svg').attr('class', 'visualization').attr('width', this.options.width).attr('height', this.options.height);

      this.treeContainer = this.svgContainer.append('g').attr('transform', 'translate(-30, ' + 100 + ')');

      this.arrayContainer = this.svgContainer.append('g').attr('transform', 'translate(60, ' + 40 + ')');

      this.arrayIndexesContainer = this.svgContainer.append('g').attr('transform', 'translate(60, ' + 10 + ')');

      var data = this._data;
      var options = this.options;
      var gCells = this.arrayIndexesContainer.selectAll('g').data(data, function (d) {
        return d.key;
      });
      var gEnter = gCells.enter().append('g');
      gEnter.append('text');
      gCells.exit().remove();

      gCells.attr('class', function () {
        var classes = ['array-cell-index'];
        return classes.join(' ');
      }).transition().duration(0).attr('transform', function (d, i) {
        return 'translate(' + (options.arrayCellWidth + 3) * i + ')';
      });

      var text = gCells.selectAll('text').transition().text(function (d) {
        return d.key;
      }).attr('x', options.arrayCellWidth / 2).attr('y', options.arrayCellHeight / 2 + 5).style("text-anchor", "middle");
    }
  }, {
    key: 'clearFocus',
    value: function clearFocus() {
      this._focusedIndexes = {};
    }
  }, {
    key: 'getFocused',
    value: function getFocused() {
      return Object.keys(this._focusedIndexes);
    }
  }, {
    key: 'focus',
    value: function focus(indexes) {
      var _this = this;

      indexes.forEach(function (i) {
        return _this._focusedIndexes[i] = true;
      });
    }
  }, {
    key: 'swap',
    value: function swap(index1, index2) {
      // swap values
      var elem1 = this._data[index1];
      this._data[index1] = this._data[index2];
      this._data[index2] = elem1;
    }
  }, {
    key: 'setActiveLength',
    value: function setActiveLength(val) {
      this.activeLength = val;
    }
  }, {
    key: 'renderArray',
    value: function renderArray(animationSpeed) {
      var _this2 = this;

      if (animationSpeed === undefined) {
        animationSpeed = this.options.animationSpeed;
      }

      var data = this._data;
      var gCells = this.arrayContainer.selectAll('g').data(data, function (d) {
        return d.key;
      });
      var gEnter = gCells.enter().append('g');
      gEnter.append('rect');
      gEnter.append('text');
      gCells.exit().remove();

      gCells.attr('class', function (d, i) {
        var classes = ['array-cell'];
        if (_this2._focusedIndexes[i] && i < _this2.activeLength) classes.push('focused');
        if (i >= _this2.activeLength) classes.push('inactive');
        return classes.join(' ');
      }).transition().duration(animationSpeed).attr('transform', function (d, i) {
        return 'translate(' + (_this2.options.arrayCellWidth + 3) * i + ')';
      });

      var rects = gCells.selectAll('rect').transition().attr('width', this.options.arrayCellWidth).attr('height', this.options.arrayCellHeight);

      var text = gCells.selectAll('text').transition().text(function (d) {
        return d.val;
      }).attr('x', this.options.arrayCellWidth / 2).attr('y', this.options.arrayCellHeight / 2 + 5).style("text-anchor", "middle");
    }
  }, {
    key: 'renderTree',
    value: function renderTree(animationSpeed) {
      var _this3 = this;

      if (animationSpeed === undefined) {
        animationSpeed = this.options.animationSpeed;
      }

      var circleRadius = this.options.treeCirclesRadius;
      var width = this.options.treeWidth - circleRadius * 2;

      var data = this._data.slice(0, this.activeLength).map(function (d, i) {
        var level = Math.ceil(Math.log2(i + 2)) - 1;
        var levelCount = Math.pow(2, level);
        var pos = i - (levelCount - 1);
        var nodeX = width / levelCount * pos + width / levelCount / 2;
        var nodeY = level * (circleRadius * 2 + _this3.options.treeLevelsVerticalPadding);
        var parentIndex = Math.trunc((i - 1) / 2);
        return { val: d.val, key: d.key, index: i, level: level, pos: pos, nodeX: nodeX, nodeY: nodeY, parentIndex: parentIndex };
      });

      var innerContEnter = this.treeContainer.selectAll('g').data([0]).enter().append('g').attr('class', 'tree-inner-cont').attr('transform', function (d) {
        return 'translate(' + (circleRadius + 5) + ', ' + (circleRadius + 5) + ')';
      });

      innerContEnter.append('g').attr('class', 'lines-cont');
      innerContEnter.append('g').attr('class', 'nodes-cont');
      var cont = this.treeContainer.select('g.tree-inner-cont');

      var lines = cont.select('.lines-cont').selectAll('line').data(data);

      lines.enter().append('line').attr('x1', 0).attr('y1', 0).attr('x2', 0).attr('y2', 0);
      lines.exit().remove();

      lines.filter(function (d) {
        return d.index != 0;
      }).transition().duration(animationSpeed).attr('x1', function (d) {
        return d.nodeX;
      }).attr('y1', function (d) {
        return d.nodeY;
      }).attr('x2', function (d) {
        return data[d.parentIndex].nodeX;
      }).attr('y2', function (d) {
        return data[d.parentIndex].nodeY;
      }).style('stroke', 'black').style('stroke-width', 1);

      var nodes = cont.select('.nodes-cont').selectAll('g').data(data, function (d) {
        return d.key;
      });
      var gEnter = nodes.enter().append("g");
      gEnter.append('line');
      gEnter.append('circle');
      gEnter.append('text');

      nodes.exit().remove();

      nodes.attr('class', function (d) {
        return _this3._focusedIndexes[d.index] ? 'tree-node focused' : 'tree-node';
      }).transition().duration(animationSpeed).attr('transform', function (d) {
        return 'translate(' + d.nodeX + ', ' + d.nodeY + ')';
      });

      nodes.select('text').transition().duration(animationSpeed).text(function (d) {
        return d.val + ' (' + d.index + ')';
      }).attr('y', 5).style("text-anchor", "middle");

      nodes.select('circle').transition().duration(animationSpeed).attr("cx", 0).attr("cy", 0).attr("r", circleRadius);
    }
  }, {
    key: 'render',
    value: function render(animationSpeed) {
      this.renderArray(animationSpeed);
      this.renderTree(animationSpeed);
    }
  }]);

  return Visualizer;
}();

var VisualizationController = function () {
  function VisualizationController(mountNode, operationsList, visualizerOptions) {
    _classCallCheck(this, VisualizationController);

    this.mountNode = mountNode;
    this.operationsList = operationsList;

    var initOperation = operationsList[0];
    this.initialData = initOperation.data;
    this.currentOpIndex = 0;

    this._initContainers();
    this.visualizer = new Visualizer(this.domNode.querySelector('.visualizer-container'), this.initialData, visualizerOptions);
    this.visualizer.render();

    this._fillOperationsSelector();
    $(this.opSelector).val(this.currentOpIndex);

    this._bindListeners();

    this._showStatusMessage(initOperation);

    if (is_test === true) {
      this._selectOperation(this.operationsList.length - 1);
      // $('.operations-list').prop('hidden', true)
    }
  }

  _createClass(VisualizationController, [{
    key: '_initContainers',
    value: function _initContainers() {
      this.domNode = $('#tpl').get(0);
      this.opSelector = $(this.domNode).find('.operations-list select').get(0);
    }
  }, {
    key: '_fillOperationsSelector',
    value: function _fillOperationsSelector() {
      d3.select(this.opSelector).attr('size', 20).style('height', this.visualizer.options.height).style('width', 200).selectAll('option').data(this.operationsList).enter().append('option').attr('value', function (d, i) {
        return i;
      }).text(function (d, i) {
        var ret = '[' + d.data.join(', ') + ']: ';
        if (d.type == 'focus') {
          ret += d.first_key + ' ' + op_symbol + ' ' + d.second_key + '?';
        }
        if (d.type == 'swap') {
          ret += d.first_key + ' <-> ' + d.second_key;
        }
        if (d.type == 'init') {
          ret += 'Создан';
        }
        if (d.type == 'change-active-length') {
          ret += 'Готово';
        }

        return i + '. ' + ret;
      });
    }
  }, {
    key: '_buttonAnswerEvent',
    value: function _buttonAnswerEvent(e) {
      console.log("before post");
      console.log(this);
      var this5 = this;
        var form = $("#test-question-form"),
            url = form.attr( "action" );
        var data = new FormData(form[0]);
        var posting = $.ajax({
          url: url,
          type: 'POST',
          data:  data,
          mimeType:"multipart/form-data",
          contentType: false,
          cache: false,
          processData:false,

          success: function(data, textStatus, jqXHR) {
            console.log("Success");
            data = JSON.parse(data);
            if (data.redirect)  {
              window.location.replace(data.redirect)
            }
            $("#updatable").html(data.status_include);
            $("#updatable-form").html(data.form_include);
            $("#modal-test-question").modal('toggle');
            this5.operationsList = JSON.parse(data.operations_list);
            this5._fillOperationsSelector();
            this5._selectOperation(this5.operationsList.length - 1);
          },
          error: function(jqXHR, textStatus, errorThrown) {
            console.log("Error");
            data = JSON.parse(jqXHR.responseText);
            if (data.redirect)  {
              window.location.replace(data.redirect)
            }
            $("#updatable-form").html(data.form_include);
          }
          
          });
        console.log("after post");
        console.log(posting);
      }
  }, {
    key: '_bindListeners',
    value: function _bindListeners() {
      var _this4 = this;

      $(this.opSelector).on('change', function () {
        var opIndex = +$(_this4.opSelector).find('option:selected').val();
        _this4._selectOperation(opIndex);
      });

      var controls = $('.control-panel');

      controls.find('button.reset').on('click', function () {
        _this4._selectOperation(0);
      });

      controls.find('button.next').on('click', function () {
        _this4._selectOperation(_this4.currentOpIndex + 1);
      });

      controls.find('button.prev').on('click', function () {
        _this4._selectOperation(_this4.currentOpIndex - 1);
      });

      var playBtn = controls.find('button.play');

      playBtn.on('click', function () {
        var stop = function stop() {
          clearInterval(_this4._playTimer);
          _this4._playTimer = null;
          playBtn.children().replaceWith('<i class="fa fa-play fa-fw">');
        };

        if (_this4._playTimer) {
          stop();
        } else {
          playBtn.children().replaceWith('<i class="fa fa-pause fa-fw">');

          _this4._playTimer = setInterval(function () {
            if (!_this4._playTimer || _this4.currentOpIndex >= _this4.operationsList.length - 1) {
              stop();
              return;
            }
            _this4._selectOperation(_this4.currentOpIndex + 1);
          }, _this4.visualizer.options.animationSpeed);
        }
      });
      $('#answer-button').on('click', function(e) {
        _this4._buttonAnswerEvent(e);
      });
    }
  }, {
    key: '_showStatusMessage',
    value: function _showStatusMessage(op) {
      var statusMessage = $(this.domNode).find('.status-message p');
      statusMessage.text(op.message);

    }
  }, {
    key: '_makeOp',
    value: function _makeOp(op, direction) {
      if (op.type == 'swap') {
        this.visualizer.clearFocus();
        this.visualizer.focus([op.first_index, op.second_index]);
        this.visualizer.swap(op.first_index, op.second_index);
        if (op.change_active_length != undefined) {
          var change_active_length = op.change_active_length;
          if (direction == 'backward') {
            change_active_length = -change_active_length;
          }
          this.visualizer.setActiveLength(
              this.visualizer.activeLength + change_active_length);
        }

      }
      if (op.type == 'focus') {
        this.visualizer.clearFocus();
        if (direction == 'forward') {
          this.visualizer.focus([op.first_index, op.second_index]);
        }
      }
      if (op.type == 'init') {
        this.visualizer.clearFocus();
        this.visualizer.setInitialData(op.data);
      }
      if (op.type == 'change-active-length') {
        this.visualizer.clearFocus();
        var step = op.step;
        if (direction == 'backward') {
          step = -step;
        }
        this.visualizer.setActiveLength(this.visualizer.activeLength + step);
      }
      this._lastOperation = op;
      this._lastOperationTime = new Date();
    }
  }, {
    key: '_jumpTo',
    value: function _jumpTo(opIndex) {
      var currentOpIndex = this.currentOpIndex;

      var direction = currentOpIndex < opIndex ? 'forward' : 'backward';
      if (currentOpIndex < opIndex) {
        for (var i = currentOpIndex + 1; i <= opIndex; i++) {
          this._makeOp(this.operationsList[i], direction);
        }
      } else {
        for (var _i = currentOpIndex; _i > opIndex; _i--) {
          this._makeOp(this.operationsList[_i], direction);
        }
        var lastOp = this.operationsList[opIndex];
        if (lastOp.type == 'focus') {
          this._makeOp(lastOp, 'forward');
        }
      }
      this.currentOpIndex = opIndex;
    }
  }, {
    key: '_selectOperation',
    value: function _selectOperation(opIndex) {
      if (opIndex < 0) {
        opIndex = 0;
      }
      if (opIndex >= this.operationsList.length) {
        opIndex = this.operationsList.length - 1;
      }

      var lastOpTime = this._lastOperationTime;

      $(this.opSelector).val(opIndex);
      this._jumpTo(opIndex);
      this._showStatusMessage(this.operationsList[opIndex]);

      var delta = new Date() - lastOpTime;

      if (delta > this.visualizer.options.animationSpeed - 50) {
        this.visualizer.render();
      } else {
        this.visualizer.render(0);
      }
    }
  }]);

  return VisualizationController;
}();

window.c = new VisualizationController(
    document.querySelector('#root'),
    OperationsList, {
      height: 345,
      width: 560,
      animationSpeed: 1500,
      treeCirclesRadius: 20,
});
window.$ = $;
